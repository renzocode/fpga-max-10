# maxprologic-fpga MAX 10

## Get MAXPROLOGIC_FPGA_PROJECT_2.7_DVD

[MaxPrologic-fpga-2.7](https://drive.google.com/file/d/1dSAJ1-tB3NQUUTgZcA8crLucarJ4B0Z-/view?usp=sharing).

## Get documentation

Documentation folder.

Write the directory name.

![alt text](./Imagenes/imagen-01.png)

Choose empty project.

![alt text](./Imagenes/imagen-02.png)

Choose FPGA architecture.

![alt text](./Imagenes/imagen-03.png)

Select Simulation and system verilog HDL.

![alt text](./Imagenes/imagen-04.png)

Select File->New.

![alt text](./Imagenes/imagen-05.png)

Save the file as default name project.

![alt text](./Imagenes/imagen-06.png)