-- This is a VHDL template for use with the BeMicro MAX 10 development kit    
-- It is used for showing the IO pin names and directions                     
-- Ver 0.1 10.07.2014                                                         

-- The signals below are documented in the BeMicro MAX 10 Getting Started     
-- User Guide.  Please refer to that document for additional information.     

-- NOTE: A Verilog version of this template is also provided with this design
-- example for users that prefer Verilog. This leds.vhd file
-- would need to be removed from the project and replaced with the
-- leds.v file to switch to the Verilog template.

-- The signals below are documented in the "BeMicro MAX 10 Getting Started
-- User Guide."  Please refer to that document for additional signal details.

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.all;

entity leds is
port(
	-- Clock ins, SYS_CLK = 50MHz, USER_CLK = 24MHz 
	SYS_CLK : in std_logic;
	
	-- LED outs 
	USER_LED : out std_logic_vector(8 downto 1)
);

end entity leds;

architecture arch of leds is
	signal divider_counter : unsigned(23 downto 0) := (others =>'0');
	signal state: unsigned(3 downto 0) := (others =>'0');
	signal scanningLED: std_logic_vector(8 downto 1);
begin
	divider_p: process (SYS_CLK)
		begin
			if (rising_edge(SYS_CLK)) then
				if (divider_counter = to_unsigned(9999999,24)) then
					divider_counter <= (others =>'0');
					if (state = to_unsigned(14,4)) then
						state <= to_unsigned(1,4);
					else
						state <= state + 1;
					end if;
				else
					divider_counter <= divider_counter + 1;
					state <= state;
				end if;
			end if;
		end process;
	led_driver_p: process (state)
		begin
			case to_integer(state) is
				when 1 => scanningLED <= "10000000";
				when 2|14 => scanningLED <= "01000000";
				when 3|13 => scanningLED <= "00100000";
				when 4|12 => scanningLED <= "00010000";
				when 5|11 => scanningLED <= "00001000";
				when 6|10 => scanningLED <= "00000100";
				when 7|9 => scanningLED <= "00000010";
				when 8 => scanningLED <= "00000001";
				when others => scanningLED <= "00000000";
			end case;
		end process;
	USER_LED <= not scanningLED;
end architecture arch;